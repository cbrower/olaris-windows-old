﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Olaris.Controls
{
    public sealed partial class Poster : UserControl, INotifyPropertyChanged
    {
        private string _posterPath;
        private bool _watched;
        private double _progress;
        private int _fileCount;

        // Full URL path to the item's poster image
        public string PosterPath
        {
            get => _posterPath;
            set
            {
                _posterPath = value;
                NotifyPropertyChanged("PosterPath");
            }
        }

        public bool Watched
        {
            get => _watched;
            set
            {
                _watched = value;
                NotifyPropertyChanged("Watched");
            }
        }

        // 0 = 0%, 1 = 100%
        public double Progress
        {
            get => _progress;
            set
            {
                _progress = value;
                NotifyPropertyChanged("Progress");
                NotifyPropertyChanged("ProgressBarVisibility");
            }
        }

        public int FileCount
        {
            get => _fileCount;
            set
            {
                _fileCount = value;
                NotifyPropertyChanged("FileCount");
                NotifyPropertyChanged("FileCountVisibility");
            }
        }

        // Only make the progress bar visible if the item is partially watched.
        public Visibility ProgressBarVisibility
        {
            get
            {
                if (_progress > 0 && _progress < 1) { return Visibility.Visible; }
                else { return Visibility.Collapsed; }
            }
        }

        // Only make the file count visible if it's greater than 1.
        public Visibility FileCountVisibility
        {
            get
            {
                if (_fileCount > 1) { return Visibility.Visible; }
                else { return Visibility.Collapsed; }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public Poster()
        {
            InitializeComponent();
        }
    }
}
