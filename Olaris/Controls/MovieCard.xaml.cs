﻿using Olaris.Models;
using System.Linq;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Olaris.Controls
{
    public sealed partial class MovieCard : UserControl
    {
        private Movie _movie;
        public Movie Movie
        {
            get => _movie;
            set
            {
                // Set the poster paths as absolute URLs
                if (value.PosterPath != "" && value.PosterPath != null)
                {
                    PosterPath = ((App)Application.Current).State.GetFullPosterPath(value.PosterPath);
                }
                else
                {
                    PosterPath = (string)Application.Current.Resources["DefaultPosterPath"];
                }

                // Calculate the progress
                Progress = value.PlayState.PlayTime / value.Files.First().TotalDuration;

                _movie = value;
            }
        }
        public string PosterPath { get; set; }
        public double Progress { get; set; }

        public MovieCard()
        {
            InitializeComponent();
        }
    }
}
