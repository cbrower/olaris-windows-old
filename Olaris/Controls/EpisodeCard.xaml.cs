﻿using Olaris.Models;
using System.Linq;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Olaris.Controls
{
    public sealed partial class EpisodeCard : UserControl
    {
        private Episode _episode;
        public Episode Episode
        {
            get => _episode;
            set
            {
                // Set the poster paths as absolute URLs
                if (value.Season.PosterPath != "" && value.Season.PosterPath != null)
                {
                    PosterPath = ((App)Application.Current).State.GetFullPosterPath(value.Season.PosterPath);
                }
                else if (value.Season.Series.PosterPath != "" && value.Season.Series.PosterPath != null)
                {
                    PosterPath = ((App)Application.Current).State.GetFullPosterPath(value.Season.Series.PosterPath);
                }
                else
                {
                    PosterPath = (string)Application.Current.Resources["DefaultPosterPath"];
                }

                // Calculate the progress
                this.Progress = value.PlayState.PlayTime / value.Files.First().TotalDuration;

                _episode = value;
            }
        }
        public string PosterPath { get; set; }
        public double Progress { get; set; }

        public EpisodeCard()
        {
            this.InitializeComponent();
        }
    }
}
