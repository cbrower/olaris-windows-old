﻿using Flurl;
using Flurl.Http;
using System.Threading.Tasks;

namespace Olaris.Auth
{
    class Request
    {
        string host;
        string username;
        string password;

        public Request(string host, string username, string password)
        {
            this.host = host;
            this.username = username;
            this.password = password;
        }

        private Url Endpoint()
        {
            return host.AppendPathSegment("/olaris/m/v1/auth");
        }

        public async Task<string> Execute()
        {
            var url = this.Endpoint();

            var resp = await url.PostJsonAsync(new
            {
                this.username,
                this.password
            }).ReceiveJson<Response>();

            return resp.Jwt;
        }
    }

    public class Response
    {
        public string Jwt { get; set; }

        public Response() { }

    }
}
