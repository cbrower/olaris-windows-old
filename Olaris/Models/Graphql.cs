﻿using System.Collections.Generic;

namespace Olaris.Models
{
    public class GraphqlRequest
    {
        public string OperationName { get; set; }
        public string Query { get; set; }
        public Dictionary<string, string> Variables { get; set; }

        public GraphqlRequest(string o, string q, Dictionary<string, string> v)
        {
            OperationName = o;
            Query = q;
            Variables = v;
        }
    }

    public class GraphqlResponse<T>
    {
        public T Data { get; set; }

        public GraphqlResponse(T val)
        {
            Data = val;
        }
    }
}
