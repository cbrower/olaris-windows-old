﻿namespace Olaris.Models
{
    public class Stream
    {
        public string CodecName { get; set; }
        public string CodecMime { get; set; }
        public string Profile { get; set; }
        public int BitRate { get; set; }
        public string StreamType { get; set; }
        public string Language { get; set; }
        public string Title { get; set; }
        public string Resolution { get; set; }
        public float TotalDuration { get; set; }
        public int StreamId { get; set; }
        public string StreamUrl { get; set; }
    }
}
