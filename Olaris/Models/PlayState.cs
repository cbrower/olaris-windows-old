﻿namespace Olaris.Models
{
    public class PlayState
    {
        public bool Finished { get; set; }
        public float PlayTime { get; set; }
        public string Uuid { get; set; }
    }
}
