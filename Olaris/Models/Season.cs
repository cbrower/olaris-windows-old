﻿namespace Olaris.Models
{
    public class Season
    {
        public string Name { get; set; }
        public string Overview { get; set; }
        public int SeasonNumber { get; set; }
        public string AirDate { get; set; }
        public string PosterPath { get; set; }
        public int TmdbId { get; set; }
        public Episode[] Episodes { get; set; }
        public string Uuid { get; set; }
        public int UnwatchedEpisodesCount { get; set; }
        public Series Series { get; set; }
    }
}
