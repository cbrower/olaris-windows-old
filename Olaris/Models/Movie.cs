﻿using System.Collections.Generic;

namespace Olaris.Models
{
    public class Movie : MediaItem
    {
        public string Title { get; set; }
        public string Year { get; set; }
        public string ImdbId { get; set; }
        public string BackdropPath { get; set; }
        public string PosterPath { get; set; }
        public List<MovieFile> Files { get; set; }
    }
}
