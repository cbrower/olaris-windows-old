﻿using System.Collections.Generic;

namespace Olaris.Models
{
    public class Episode : MediaItem
    {
        public string StillPath { get; set; }
        public string AirDate { get; set; }
        public int EpisodeNumber { get; set; }
        public List<EpisodeFile> Files { get; set; }
        public Season Season { get; set; }
    }
}
