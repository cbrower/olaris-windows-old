﻿namespace Olaris.Models
{
    public class Series
    {
        public string Name { get; set; }
        public string Overview { get; set; }
        public string FirstAirDate { get; set; }
        public string Status { get; set; }
        public Season[] Seasons { get; set; }
        public string BackdropPath { get; set; }
        public string PosterPath { get; set; }
        public int TmdbId { get; set; }
        public string Type { get; set; }
        public string Uuid { get; set; }
        public int UnwatchedEpisodesCount { get; set; }
    }
}
