﻿namespace Olaris.Models
{
    // A MediaItem represents some kind of media on the server, either a movie
    // or a TV episode
    public class MediaItem
    {
        public string Name { get; set; }
        public string Uuid { get; set; }
        public string Overview { get; set; }
        public int TmdbId { get; set; }
        public PlayState PlayState { get; set; }
    }
}
