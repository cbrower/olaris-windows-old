﻿namespace Olaris.Models
{
    public class EpisodeFile
    {
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string uuid { get; set; }
        public Stream[] Streams { get; set; }
        public float TotalDuration { get; set; }
        public int FileSize { get; set; }
        public Library Library { get; set; }

    }
}
