﻿using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml;
using Olaris.Queries;
using Olaris.Models;
using Olaris.Controls;

namespace Olaris.Views
{
    /// <summary>
    /// The dashboard that shows next episodes and recently added content
    /// </summary>
    public sealed partial class Dashboard : Page
    {

        public Dashboard()
        {
            this.InitializeComponent();
            this.LoadMedia();
        }

        // Populates the "Up Next" list on the Dashboard
        private async void LoadUpNext()
        {
            UpNext upNextQuery = new UpNext();
            var items = await upNextQuery.Execute(
                ((App)Application.Current).State.GetServerUrl(),
                ((App)Application.Current).State.GetToken()
            );
            
            foreach (MediaItem item in items)
            {
                if (item is Movie)
                {
                    var movieCard = new MovieCard();
                    movieCard.Movie = (Movie)item;
                    this.UpNextGridView.Items.Add(movieCard);
                }
                if (item is Episode)
                {
                    var episodeCard = new EpisodeCard();
                    episodeCard.Episode = (Episode)item;
                    this.UpNextGridView.Items.Add(episodeCard);
                }
            }
        }

        // Populates the recently added movie/episode lists
        private async void LoadRecentlyAdded()
        {
            RecentlyAdded query = new RecentlyAdded();
            (var movies, var episodes) = await query.Execute(
                ((App)Application.Current).State.GetServerUrl(),
                ((App)Application.Current).State.GetToken()
            );

            foreach (Movie movie in movies)
            {
                var movieCard = new MovieCard();
                movieCard.Movie = movie;
                this.RecentMovies.Items.Add(movieCard);
            }

            foreach (Episode episode in episodes)
            {
                var episodeCard = new EpisodeCard();
                episodeCard.Episode = episode;
                this.RecentEpisodes.Items.Add(episodeCard);
            }
        }

        // Refreshes the auth token and then fetches the recently added and up
        // next listings
        private async void LoadMedia()
        {
            await ((App)Application.Current).State.RefreshToken();
            this.LoadUpNext();
            this.LoadRecentlyAdded();
        }

        private void UpNext_ItemClick(object sender, ItemClickEventArgs e)
        {
            var selectedItem = e.ClickedItem;
            if (selectedItem is MovieCard)
            {
                this.Frame.Navigate(typeof(MovieDetails), (selectedItem as MovieCard).Movie.Uuid);
            }
        }

        private void RecentMovies_ItemClick(object sender, ItemClickEventArgs e)
        {
            var selectedItem = e.ClickedItem as MovieCard;
            this.Frame.Navigate(typeof(MovieDetails), selectedItem.Movie.Uuid);
        }

        private void RecentEpisodes_ItemClick(object sender, ItemClickEventArgs e)
        {
            // TODO
        }
    }
}