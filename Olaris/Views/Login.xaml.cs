﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Flurl;
using Flurl.Http;
using Olaris.Models;

namespace Olaris.Views
{
    /// <summary>
    /// The dashboard that shows next episodes and recently added content
    /// </summary>
    public sealed partial class Login : Page
    {
        public Login()
        {
            this.InitializeComponent();
        }

        private void ShowLoadingAnimation()
        {
            LoadingIndicator.IsActive = true;
            LoginText.Visibility = Visibility.Collapsed;
        }

        private void HideLoadingAnimation()
        {
            LoginText.Visibility = Visibility.Visible;
            LoadingIndicator.IsActive = false;
        }

        // Test the credentials given, and if they work, save them in the app
        // state and continue to the main page.
        private async void Continue_OnClick(object sender, RoutedEventArgs e)
        {
            this.ShowLoadingAnimation();

            string host = ServerUrl.Text;
            string username = UserId.Text;
            string password = Password.Password;
            Auth.Request req;
            Auth.Response res;

            // Build the Auth Request - this will throw an error if the URL fails to build
            try
            {
                req = new Auth.Request(host, username, password);
            }
            catch
            {
                NotificationBox.Show("Server URL is not valid", 5000);
                this.HideLoadingAnimation();
                return;
            }

            // Send the request and return the JWT
            try
            {
                var jwt = await req.Execute();
            }
            catch
            {
                NotificationBox.Show("Login failed", 5000);
                this.HideLoadingAnimation();
                return;
            }

            this.SaveCredentials(host, username, password);
            ((App)Application.Current).GoToMainPage();
        }

        private void SaveCredentials(string server, string username, string password)
        {
            var vault = new Windows.Security.Credentials.PasswordVault();
            vault.Add(new Windows.Security.Credentials.PasswordCredential(
                server, username, password));
            ((App)Application.Current).State.SetDefaultServer(server, username, true);
        }
    }
}