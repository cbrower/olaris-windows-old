﻿using System.Linq;
using Windows.UI.Core;
using Windows.UI.Xaml.Controls;

namespace Olaris.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        
        public MainPage()
        {
            InitializeComponent();
            SetNavigation();
            SystemNavigationManager.GetForCurrentView().BackRequested += MainPage_BackRequested;
        }

        // Handle back button events. If there is a previous page to go back
        // to, go back and mark the event as handled.
        private void MainPage_BackRequested(object sender, BackRequestedEventArgs e)
        {
            if (ContentFrame.BackStack.Any())
            {
                e.Handled = true;
                ContentFrame.GoBack();
            }
        }

        private void SetNavigation()
        {
            if (ContentFrame.Content == null)
            {
                ContentFrame.Navigate(typeof(Dashboard));
            }
        }

        // Handles the user clicking on navigation menu items
        private void NavView_OnItemInvoked(NavigationView sender, NavigationViewItemInvokedEventArgs args)
        {
            switch (args.InvokedItem as string)
            {
                case "Dashboard":
                    ContentFrame.Navigate(typeof(Dashboard));
                    break;
                case "Movies":
                    break;
                case "TV Shows":
                    break;
                case "Users":
                    break;
            }
        }

        private void ContentFrame_Navigated(object sender, Windows.UI.Xaml.Navigation.NavigationEventArgs e)
        {
            // Make the back button visible if it's possible to go back in the
            // content frame
            SystemNavigationManager.GetForCurrentView()
                .AppViewBackButtonVisibility = ContentFrame.BackStack.Any() ?
                AppViewBackButtonVisibility.Visible :
                AppViewBackButtonVisibility.Collapsed;

            // Set the active item on the navigation menu if the current frame
            // content matches a menu item.
            string pageName = ContentFrame.Content.GetType().Name;
            string[] navigationNames = { "Dashboard" };

            if (navigationNames.Contains(pageName))
            {
                NavView.SelectedItem = NavView.MenuItems
                    .OfType<NavigationViewItem>()
                    .Where(item => item.Tag.ToString() == pageName)
                    .First();
            }
            else
            {
                NavView.SelectedItem = null;
            }
        }
    }
}