﻿using Olaris.Models;
using Olaris.Queries;
using System;
using System.Linq;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

namespace Olaris.Views
{
    /// <summary>
    /// View that shows a movie's information
    /// </summary>
    public sealed partial class MovieDetails : Page
    {

        public Movie Movie { get; set; }
        public string Poster { get; set; } = (string)Application.Current.Resources["DefaultPosterPath"];
        public double Progress { get; set; } = 0;

        public MovieDetails()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            var uuid = (string)e.Parameter;
            GetMovieDetails(uuid);
        }

        private async void GetMovieDetails(string uuid)
        {
            MovieQuery q = new MovieQuery(uuid);
            var m = await q.Execute(
                ((App)Application.Current).State.GetServerUrl(),
                ((App)Application.Current).State.GetToken()
            );
            Movie = m;
            Poster = ((App)Application.Current).State.GetFullPosterPath(m.PosterPath);
            Progress = Movie.PlayState.PlayTime / Movie.Files.First().TotalDuration;
            Bindings.Update();
        }

        // When the file selected is changed, update the metadata
        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var c = sender as ComboBox;
            var moviefile = (MovieFile)c.SelectedItem;

            SubtitleList.Text = string.Join(" ", moviefile.SubtitleList());
            AudioTrackList.Text = string.Join(" ", moviefile.AudioTrackList());
            Resolution.Text = moviefile.Streams.First(x => x.StreamType == "video").Resolution;
            Runtime.Text = Math.Truncate(moviefile.TotalDuration / 60) + " minutes";
        }
    }
}
