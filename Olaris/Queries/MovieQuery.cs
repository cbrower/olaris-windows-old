﻿using Flurl;
using Flurl.Http;
using Newtonsoft.Json.Linq;
using Olaris.Models;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Olaris.Queries
{
    class MovieQuery
    {

        private string uuid;

        string query = @"query movies($uuid: String!) {
            movies(uuid: $uuid) {
                type: __typename
                name
                year
                overview
                imdbID
                backdropPath
                uuid
                posterPath

                playState {
                    finished
                    playtime
                }

                files {
                    fileName
                    filePath
                    uuid
                    totalDuration
                    library {
                        healthy
                    }
                    streams {
                        codecMime
                        streamType
                        resolution
                        bitRate
                        language
                    }
                }
            }
        }";

        public MovieQuery(string uuid)
        {
            this.uuid = uuid;
        }

        private Url Endpoint(string host)
        {
            return host.AppendPathSegment("/olaris/m/query");
        }

        public async Task<Movie> Execute(string host, string Token)
        {
            GraphqlRequest req = new GraphqlRequest(null, query, new Dictionary<string, string> {
                { "uuid", uuid }
            });
            HttpResponseMessage resp = await this.Endpoint(host)
                .WithHeader("Authorization", $"bearer {Token}")
                .PostJsonAsync(req);

            string response_text = await resp.Content.ReadAsStringAsync();
            JObject root = JObject.Parse(response_text);

            // These are the values that will be returned
            List<Movie> items = new List<Movie>();

            Movie m = root["data"]["movies"].ToList().First().ToObject<Movie>();
            return m;
        }
    }
}
