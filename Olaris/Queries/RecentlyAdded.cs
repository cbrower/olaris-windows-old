﻿using Flurl;
using Flurl.Http;
using Newtonsoft.Json.Linq;
using Olaris.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Olaris.Queries
{
    public class RecentlyAdded
    {
        string query = @"{
            recentlyAdded {
                ... on Movie {
                    type: __typename
                    uuid
                    name
                    year
                    posterPath

                    playState {
                        finished
                        playtime
                    }

                    files {
                        totalDuration
                    }
                }
                ... on Episode {
                    type: __typename
                    uuid
                    name
                    episodeNumber

                    season {
                        seasonNumber
                        posterPath
                        uuid

                        series {
                            name
                            uuid
                        }
                    }

                    playState {
                        finished
                        playtime
                    }

                    files {
                        totalDuration
                    }
                }
            }
        }";

        private Url Endpoint(string host)
        {
            return host.AppendPathSegment("/olaris/m/query");
        }

        public async Task<(List<Movie>, List<Episode>)> Execute(string host, string Token)
        {
            GraphqlRequest req = new GraphqlRequest(null, query, null);
            HttpResponseMessage resp = await this.Endpoint(host)
                .WithHeader("Authorization", $"bearer {Token}")
                .PostJsonAsync(req);

            string response_text = await resp.Content.ReadAsStringAsync();
            JObject root = JObject.Parse(response_text);

            // These are the values that will be returned
            List<Movie> movies = new List<Movie>();
            List<Episode> episodes = new List<Episode>();

            // Deserialize each element from the recentlyAdded array as either
            // a Movie or an Episode
            foreach (JToken item in root["data"]["recentlyAdded"].ToList())
            {
                string t = item["type"].Value<string>().ToLower();
                switch (t)
                {
                    case "movie":
                        Movie m = item.ToObject<Movie>();
                        movies.Add(m);
                        break;
                    case "episode":
                        Episode e = item.ToObject<Episode>();
                        episodes.Add(e);
                        break;
                    default:
                        throw new ApplicationException(String.Format("The media item type '{0}' is not supported!", t));

                }
            }

            return (movies, episodes);
        }
    }
}
