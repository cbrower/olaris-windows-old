﻿using Flurl;
using Flurl.Http;
using Newtonsoft.Json.Linq;
using Olaris.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Olaris.Queries
{
    public class UpNext
    {
        string query = @"{
            upNext {
                ... on Movie {
                    type: __typename
                    uuid
                    name
                    year
                    posterPath

                    playState {
                        finished
                        playtime
                    }

                    files {
                        totalDuration
                    }
                }
                ... on Episode {
                    type: __typename
                    uuid
                    name
                    episodeNumber

                    season {
                        seasonNumber
                        posterPath
                        uuid

                        series {
                            uuid
                            name
                            posterPath
                        }
                    }

                    playState {
                        finished
                        playtime
                    }

                    files {
                        totalDuration
                    }
                }
            }
        }";

        private Url Endpoint(string host)
        {
            return host.AppendPathSegment("/olaris/m/query");
        }

        public async Task<List<MediaItem>> Execute(string host, string Token)
        {
            GraphqlRequest req = new GraphqlRequest(null, query, null);
            HttpResponseMessage resp = await this.Endpoint(host)
                .WithHeader("Authorization", $"bearer {Token}")
                .PostJsonAsync(req);

            string response_text = await resp.Content.ReadAsStringAsync();
            JObject root = JObject.Parse(response_text);

            // These are the values that will be returned
            List<MediaItem> items = new List<MediaItem>();

            // Deserialize each element from the upNext array
            foreach (JToken item in root["data"]["upNext"].ToList())
            {
                string t = item["type"].Value<string>().ToLower();
                switch (t)
                {
                    case "movie":
                        Movie m = item.ToObject<Movie>();
                        items.Add(m);
                        break;
                    case "episode":
                        Episode e = item.ToObject<Episode>();
                        items.Add(e);
                        break;
                    default:
                        throw new ApplicationException(String.Format("The media item type '{0}' is not supported!", t));

                }
            }

            return items;
        }
    }
}
