﻿using Flurl;
using Flurl.Http;
using System.Threading.Tasks;
using Windows.Storage;

namespace Olaris
{
    public class AppState
    {
        private string Token;
        private string ServerUrl;
        private string Username;

        public AppState()
        {
            this.LoadSettings();
        }

        // Refreshes the token. If it succeeds, it sets the token in the
        // AppState. Returns a boolean to indicate success or failure.
        public async Task<bool> RefreshToken()
        {
            var vault = new Windows.Security.Credentials.PasswordVault();
            var credentials = vault.Retrieve(this.ServerUrl, this.Username);

            var req = new Auth.Request(this.ServerUrl, this.Username, credentials.Password);
            try
            {
                this.Token = await req.Execute();
                return true;
            }
            catch
            {
                return false;
            }
        }

        // Loads settings from local settings
        private void LoadSettings()
        {
            ApplicationDataContainer localSettings = ApplicationData.Current.LocalSettings;
            ApplicationDataCompositeValue composite = (ApplicationDataCompositeValue)localSettings.Values["DefaultServerInfo"];
            if (composite != null)
            {
                this.ServerUrl = composite["Resource"] as string;
                this.Username = composite["User"] as string;
            }
        }

        // Saves the current settings to local storage
        private void SaveSettings()
        {
            ApplicationDataContainer localSettings = ApplicationData.Current.LocalSettings;
            ApplicationDataCompositeValue composite = new ApplicationDataCompositeValue();
            composite["Resource"] = this.ServerUrl;
            composite["User"] = this.Username;
            localSettings.Values["DefaultServerInfo"] = composite;
        }

        // Sets a new default server into the settings. If save is true, it
        // will save them to the application local storage.
        public void SetDefaultServer(string server, string username, bool save)
        {
            this.ServerUrl = server;
            this.Username = username;
            if (save)
            {
                this.SaveSettings();
            }
        }

        // Returns the server URL
        public string GetServerUrl()
        {
            return this.ServerUrl;
        }

        // Returns the AppState Token
        public string GetToken()
        {
            return this.Token;
        }

        // Takes a filename and builds a full URL to the poster.
        public Url GetFullPosterPath(string filename)
        {
            return this.GetServerUrl().AppendPathSegment("/olaris/m/images/tmdb/w342").AppendPathSegment(filename);
        }
    }
}
