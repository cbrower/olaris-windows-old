﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace Olaris.Helpers
{

    // Converts a bool to a Visibility so that it can be used to toggle a
    // control's visibility in XAML. Coverts true to Visible and false to
    // Collapsed.
    public class BoolToVisConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            return (value is bool && (bool)value) ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return value is Visibility && (Visibility)value == Visibility.Visible;
        }
    }

    // Converts a bool to a Visibility so that it can be used to toggle a
    // control's visibility in XAML. Coverts true to Collapsed and false to
    // Visible.
    public class BoolToVisConverterInverted : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            return (value is bool && (bool)value) ? Visibility.Collapsed : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return value is Visibility && (Visibility)value == Visibility.Collapsed;
        }
    }

}
